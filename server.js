//express
const express = require('express')
const app = express()
//http
const http = require('http')
const server = http.createServer(app)
//socket.io
const { Server } = require("socket.io")

const io = new Server(server)

usernames=[];

//midleware
app.use('/css', express.static(__dirname + '/css'));

server.listen(9090, function(){
    console.log('connexion ok')
})
//////////  /END base////////////////////////////




//////////////routes/////////////
//index
app.get('/',function(req,res){
    res.sendFile(__dirname + '/index.html')
});

const redirection = () => {app.get('/',function(req,res){
    res.sendFile(__dirname + '/index.html')
});}

//connexion
io.sockets.on('connection', function(socket){
    //add user
    socket.on('new user',function(data,callback){
        if(usernames.indexOf(data) != -1){
            callback(false);
        } else{
            callback(true);
            socket.username = data;
            usernames.push(socket.username);
            io.sockets.emit('usernames',usernames);
        }
        console.log('user connecter')
        });

    //drop user
    socket.on('disconnect', function(){
        console.log('deconnection user') 
        
    })
    // send msg
    socket.on('send message', function (data){
        io.sockets.emit('new message',{message:data,username:socket.username});
    })
    // socket.on('send message',function(data){
    // });


    console.log(socket.username)
});

